
############### VALIDATING CHARACTERS ###############################
def is_string?(value)
    value.match(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u)
end

################# END OF VALIDATING CHARACTERS ##########################


################# VALIDATING PHONE NUMBERS ############################
def mobile_number_matched?(mobile_number)
    mobile_number.match(/^(\+?)(\d){10,12}$/)
end

################## END OF VALIDATING PHONE NUMBERS ####################


################# VALIDATING PHONE NUMBERS ##########################

def validatePhoneNumber(mobile_number)
   wildcard_search = "#{mobile_number}"
   if wildcard_search[0..2]=='233' && wildcard_search.length==12
      wildcard_search=CTRYCODE+"#{wildcard_search[3..wildcard_search.length]}"
   elsif wildcard_search[0]=='0' && wildcard_search.length==10
      wildcard_search = CTRYCODE+"#{wildcard_search[1..wildcard_search.length]}"
   elsif wildcard_search[0] == '+' && wildcard_search[1..3] == '233'&& wildcard_search[4..wildcard_search.length].length == 9
      wildcard_search = CTRYCODE+"#{wildcard_search[4..wildcard_search.length]}"
   elsif wildcard_search[0] != "+" && wildcard_search[0..2]!='233' && wildcard_search.length == 9
      wildcard_search=CTRYCODE+"#{wildcard_search[0..wildcard_search.length]}"
   end
   
   return wildcard_search
end

####################### END OF VALIDATING PHONE NUMBERS ##############################################


########################## VALIDATING PIN MATCHED ###############################

def pin_matched?(pin)
    pin.match(/^\d{4}$/)
end

####################### END OF PIN VALIDATION ###################################

############ GENERATING FOUR RANDOM PIN ########################################
def fourRandomNumbers
  time=Time.new
  strtm=time.strftime("1"+"%L")
  uniq_id = "EC#{strtm}"
  return uniq_id
end
#################################################################################


def cost_cal(qty,amt)
  _data_hash = {}
  
  _amount = amt.to_f
  _quantity = qty.to_f
  
  
  _total = _amount * _quantity
  
   _data_hash = {
       'Total' => _total.to_f
   }
  
  
  _data_hash
end







class String
  def valid_float?
    !!Float(self) rescue false
  end
 def is_number?
    true if Float(self) rescue false
  end
  
   def is_letter?
    true if String(self) rescue false
  end
 def numeric?
    return true if self =~ /\A\d+\Z/
    true if Float(self) rescue false
  end
end


def ordering(full_name,occasion_id,cake_id,pastry_id,mobile_number,reference_id,network)
  Order.create!(
  
    fullname: full_name,
    occasion_id: occasion_id,
    cake_id: cake_id,
    pastry_id: pastry_id,
    mobile_number: mobile_number,
    reference_id: reference_id,
    network: network,
    status1: 0,
    paid: 0
  )
end





def payments(fullname,shape_id,cake_id,pastry_id,size_id,flavor_id,color_id,pickup_date,pickup_time,location,mobile_number,delivery,price,reference,trans_id,network,trans_type,client_id,ref_id)
  ContributionPayment.create!(
  
  fullname: fullname,
  occasion_id: shape_id,
  cake_id: cake_id,
  pastry_id: pastry_id,
  size_id: size_id,
  flavor_id: flavor_id,
  color_id: color_id,
  pickup_date: pickup_date,
  pickup_time: pickup_time,
  location: location,
  mobile_number: mobile_number,
  delivery: delivery,
  amount: price,
  reference: reference,
  trans_id: trans_id, 
  network: network,
  trans_type: trans_type,
  client_id: client_id,
  ref_id: ref_id,
  status1: 0
  )
  
end



def update_status(trans_ref,field)

 mon = ContributionPayment.where(trans_id: trans_ref)[0]
  if mon != nil
    if field == "success"
      mon.status1 = 1
      mon.save

   elsif field == "failure"
      mon.status1 = 0
      mon.save
   end
  else
    puts
    puts
    
   puts "NO TRANSACTION ID MATCH WAS FOUND"
   puts "NO TRANSACTION ID MATCH WAS FOUND"
   puts "NO TRANSACTION ID MATCH WAS FOUND"
   puts "NO TRANSACTION ID MATCH WAS FOUND"
   puts "NO TRANSACTION ID MATCH WAS FOUND"
   puts
   puts
   puts
 end

end



def update_order(ref_id,field)
  
   mon = Order.where(reference_id: ref_id)[0]
  if mon != nil
    if field == "success"
      mon.paid = 1
      mon.save

   elsif field == "failure"
      mon.paid = 0
      mon.save
   end
  else
    puts
    puts
    
   puts "NO REF ID MATCH WAS FOUND"
   puts "NO REF ID MATCH WAS FOUND"
   puts "NO REF ID MATCH WAS FOUND"
   puts "NO REF ID MATCH WAS FOUND"
   puts "NO REF ID MATCH WAS FOUND"
   puts
   puts
   puts
 end
  
  
  
end





def genUniqueCodeCredit
  time = Time.new
  strtm = time.strftime('%y%m%L%H')
  uniq_id = "ECAKE#{strtm}"
  uniq_id
end



############################### AMFP START FROM HERE  ################################################################

def mobileMoneyReq(merchant_number, customer_number, amount, callback_url, client_id, nw, trans_type,v_num,u_code)
  
 nw = getNetWithCode(nw)
 
  url = 'https://10.105.85.76:8215'
  url_endpoint = '/sendRequest'
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
  

ts=Time.now.strftime("%Y-%m-%d %H:%M:%S")
puts
puts "Hello world... with the network #{nw}"
puts
payload={
     :customer_number => customer_number,
     :reference => STRTPREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }
puts
puts "Hello world Two..."
puts
json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"
puts
puts "JSON payload: #{json_payload}"
puts
def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
signature=computeSignature(STR_SEC_KEY, json_payload)
begin
   resp = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{STR_CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        
        bidy = resp.body
        
        puts bidy.inspect
        puts resp  
        puts resp.body  
        puts "Result from AMFP: #{resp.body}"
        puts
        
        resp.body
rescue
  puts "errorrrrrrrrrrrrr"
end

end





# ################################### END OF AMFP FUNCTION ################################################

def makeRequest2AMFPOKF(customer_number, amount, callback_url, client_id, nw, trans_type,v_num,u_code)
  puts
  puts "*******************************************"
  puts
  puts nw
  
  
  
  if nw == '01'    # MTN
    
    puts "============================MTN============================="
    Thread.new do
    @@amfp_resp  = mobileMoneyReq(MTN_MERCHANT,customer_number, amount, callback_url, client_id, nw, trans_type,v_num,u_code)
    end 
  elsif nw == '02' # VODA
    Thread.new do
   @@amfp_resp = mobileMoneyReq(VOD_MERCHANT,customer_number, amount, callback_url, client_id, nw, trans_type,v_num,u_code)
   end
  elsif nw == '03' # TIG
    Thread.new do
    @@amfp_resp =mobileMoneyReq(TIG_MERCHANT,customer_number, amount, callback_url, client_id, nw, trans_type,v_num,u_code)
   end
  elsif nw == '06' # AIR
    Thread.new do
    @@amfp_resp = mobileMoneyReq(MERCHANT_NO, customer_number, amount, callback_url, client_id, nw, trans_type,v_num,u_code)
    end
  end
  
  #_json = JSON.parse(@@amfp_resp)
  #puts "---------#{ _json["resp_code"] }---------"
end



####################### END OF AMFP ###########################################



###################### SELECTION OF NETWORK ###################
def getNetWithCode(nw_code)
  network = ''

  if nw_code == '01'
    network = STR_MTN
  elsif nw_code == '02'
    network = STR_VODA
  elsif nw_code == '03'
    network = STR_TIGO
  elsif nw_code == '06'
    network = STR_AIRTEL
  end

  return network
end
####################### END OF SELECTION OF NETWORK ##############

def saveMMLog(resp_code, resp_desc, trans_ref, am_refid)
  MmLog.create!(
  resp_code: resp_code,
  resp_desc: resp_desc,
  trans_ref: trans_ref,
  am_refid: am_refid,
  
  )
end


