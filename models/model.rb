
###############  E CAKES USSD ################################
############## USSD DB CONNECTION ###################################
 
  ActiveRecord::Base.configurations["ussd_db"] = {
  :adapter  => 'postgresql',
  :database => "ecakes_db",
  :username => "richard",
  :password => "!Rich26erh",
  :host     => "localhost",
  :port     => "4044"
}



######################## USSD MODELS ####################################

class Cake < ActiveRecord::Base
  establish_connection :ussd_db
    
end
###shape is occasion
class Occasion < ActiveRecord::Base
  establish_connection :ussd_db
   
end

class Size < ActiveRecord::Base
  establish_connection :ussd_db
   
end

class Flavor < ActiveRecord::Base
  establish_connection :ussd_db
   
end


class Color < ActiveRecord::Base
  establish_connection :ussd_db
   
end


class WeddingTier < ActiveRecord::Base
  establish_connection :ussd_db
   
end

class CupcakeType < ActiveRecord::Base
  establish_connection :ussd_db
end



class Character < ActiveRecord::Base
  establish_connection :ussd_db
   
end

class Pastry < ActiveRecord::Base
  establish_connection :ussd_db
   
end

class Order < ActiveRecord::Base
   establish_connection :ussd_db
   
    before_save :capz
    def capz
     
    self.fullname = titling(fullname)
     
    end
  
  
  def titling(str)
    str_list = str.split
    str_list.map { |word| word.capitalize
    word.split("-").map{|w| w.capitalize }.join("-")
    }.join(" ")
  end
  
   
end

class ContributionPayment < ActiveRecord::Base
  establish_connection :ussd_db
  
  
   before_save :capz
    def capz
     
    self.fullname = titling(fullname)
     
    end
  
  
  def titling(str)
    str_list = str.split
    str_list.map { |word| word.capitalize
    word.split("-").map{|w| w.capitalize }.join("-")
    }.join(" ")
  end
  
    
end


class UssdLog < ActiveRecord::Base
  establish_connection :ussd_db
  
end

class Tracker < ActiveRecord::Base
  establish_connection :ussd_db
  
end

class TempStorage < ActiveRecord::Base
   establish_connection :ussd_db
    
end



class InfoStorage < ActiveRecord::Base
   establish_connection :ussd_db
    
end

class MoreStorage < ActiveRecord::Base
   establish_connection :ussd_db
    
end

class SmsLog < ActiveRecord::Base
   establish_connection :ussd_db
end

class MmLog < ActiveRecord::Base
   establish_connection :ussd_db
end



################ SAVING FUNCTIONS #########################################
def ussd_activities(pg, msg_type, ses_id, serv_key, mob_num, network_code, u_body)

 UssdLog.create!(
      page: pg, msg_type: msg_type, session_id: ses_id, msisdn: mob_num,
      service_code: serv_key, nw_code: network_code,
      ussd_body: u_body
  )

end





def tracking_fn(ses_id, fn, pg, m_num)
 
    status = 1
    Tracker.create!(session_id: ses_id, function: fn, page: pg,
                       mob_number: m_num, status: status
    )
  

end




def temp_store(values,ses_id,field)
 mon = TempStorage.where(session_id: ses_id).first
 if mon
    if field == NETWORKS_CONST
       mon.recipient_nw = values
       mon.save
     elsif field == NUMBER_CONST
       mon.recipient_no = values
       mon.save
      
     elsif field == PURPOSE_CONST 
      mon.purpose = values
      mon.save
      
     elsif field == AMOUNT_CONST 
      mon.amount = values
      mon.save
      
     end
   
   
 else
   TempStorage.create!(recipient_nw: values,session_id:ses_id,status:1)
 end
 
end



def info_store(values,ses_id,field)
 mon = InfoStorage.where(session_id: ses_id).first
 if mon
    if field == NETWORKS_CONST
       mon.recipient_nw = values
       mon.save
     elsif field == NUMBER_CONST
       mon.recipient_no = values
       mon.save
      
     elsif field == PURPOSE_CONST 
      mon.purpose = values
      mon.save
      
     elsif field == AMOUNT_CONST 
      mon.amount = values
      mon.save
      
     end
   
   
 else
   InfoStorage.create!(recipient_nw: values,session_id:ses_id,status:1)
 end
 
end


def more_store(values,ses_id,field)
 mon = MoreStorage.where(session_id: ses_id).first
 if mon
    if field == NETWORKS_CONST
       mon.recipient_nw = values
       mon.save
     elsif field == NUMBER_CONST
       mon.recipient_no = values
       mon.save
      
     elsif field == PURPOSE_CONST 
      mon.purpose = values
      mon.save
      
     elsif field == AMOUNT_CONST 
      mon.amount = values
      mon.save
      
     end
   
   
 else
   MoreStorage.create!(recipient_nw: values,session_id:ses_id,status:1)
 end
 
end